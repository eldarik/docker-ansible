FROM python:alpine3.16

RUN apk --update add openssh
RUN pip3 install ansible docker-py requests
